Транслит
========
либа для траслита кир. в лат

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vector/yii2-translit "*"
```

or add

```
"vector/yii2-translit": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \\vector\helpers\AutoloadExample::widget(); ?>```